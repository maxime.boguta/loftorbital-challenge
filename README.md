# A solution for LoftOrbital Challenge

This repository hosts a solution for the Django GraphQL temperature service challenge posted by Loft Orbital.

## Features

The chosen solution lies with Django and Django Channels for the real time requirement of the GraphQL subscription. 

* Django 3.1 service powered by Python 3.8
* PostgreSQL database
* Simple GraphQL API provided by django-graphene
* Subscription capabilities over django-channels using Redis messaging backend
* Crazy useful random temperature updates ranging from 0K to sun's surface temperature

## Getting the Source Code

Clone the repository using SSH

`git@gitlab.com:maxime.boguta/loftorbital-challenge.git`

or HTTPS

`https://gitlab.com/maxime.boguta/loftorbital-challenge.git`

## Setting up the environment

The Django service and database are running inside Docker containers, get the latest version of Docker [here](https://www.docker.com/products) (developed using Docker version 19.03.5).

### Start containers

To build then start the containers, run `docker-compose up` (`docker-compose up -d` to daemonize the processes).

### Initialize database

To apply database migrations, run `docker-compose run web ./manage.py migrate`

### Launch background tasks daemon

In order to refresh data every second, start the management command using `docker-compose run web ./manage.py refresh_temperature`

## Accessing the service

Browse to [http://localhost:8000/graphql](http://localhost:8000/graphql) to access the GraphiQL interface.

### Querying
Fetch the current temperature record using the following query

	query
	{
      currentTemperature
      {
        timestamp
        value
        unit
      }
    }
    
### Subscribing
Get real time updates on the current temperature record using the following subscription

	subscription
	{
	    currentTemperatureSubscribe
	    {
	        temperature
	        {
	            timestamp
	            value
	            unit
	        }
	    }
	}

## Tests

To run the tests, run `docker-compose run web pytest`, 5 tests should come up with a success.

## Contributing

Contributing is open, feel free to report bugs to our [issue tracker](https://gitlab.com/maxime.boguta/loftorbital-challenge/-/issues) or by submitting a PR.

PRs must be passing CI tests before being considered.




