import random

from django.db import models
from django.utils.timezone import now


def get_random_temperature_value() -> int:
    """
    Generates a random temperature from 0K to Sun surface's temperature
    """
    return random.randint(-273, 5505)


class TemperatureRecord(models.Model):
    timestamp = models.DateTimeField(default=now)  # Last time temperature value changed
    value = models.IntegerField(
        default=get_random_temperature_value
    )  # Value to be changed every second, always in Celcius

    def refresh(self):  # Updates entry's temperature value and timestamp
        self.value = get_random_temperature_value()
        self.timestamp = now()
        self.save()
