"""
Utility functions for the GraphQL temperature service
"""

from .models import TemperatureRecord
from .schema import TemperatureRecordSubscription


def refresh_record_and_notify():
    """
    Updates TemperatureRecord timestamp and temperature then notifies subscribed clients to GraphQL endpoint
    """
    try:
        TemperatureRecord.objects.get().refresh()
    except TemperatureRecord.DoesNotExist:
        TemperatureRecord.objects.create()
    TemperatureRecordSubscription.temperature_refreshed()
