"""
Management command to update TemperatureRecord data every second
"""


import time

from django.core.management.base import BaseCommand, CommandError

from temperature.utils import refresh_record_and_notify


class Command(BaseCommand):
    help = "Refreshes temperature record every second, indefinitely"

    def handle(self, *args, **options):
        while True:
            time.sleep(1)
            refresh_record_and_notify()
