"""
GraphQL schema for temperature data exposition over query and subscription
"""

import channels_graphql_ws
import graphene

from graphene_django import DjangoObjectType, DjangoListField

from .models import TemperatureRecord


class TemperatureRecordType(DjangoObjectType):
    """
    Object Type representing and resolving a TemperatureRecord
    """

    unit = graphene.String()

    class Meta:
        model = TemperatureRecord
        fields = ("timestamp", "value")

    def resolve_unit(parent, info):
        return "Celcius"


class TemperatureRecordSubscription(channels_graphql_ws.Subscription):
    """
    Subscription class for real time handling of TemperatureRecord changes
    """

    temperature = graphene.Field(TemperatureRecordType)

    def publish(self, info):
        return TemperatureRecordSubscription(
            temperature=TemperatureRecord.objects.get()
        )  # Get the freshest data

    def subscribe(self, info):
        return ["TemperatureRecordSubscription"]

    @classmethod
    def temperature_refreshed(cls):
        cls.broadcast(group="TemperatureRecordSubscription", payload={})


class Query(graphene.ObjectType):
    """
    Query definition class
    """

    currentTemperature = graphene.Field(TemperatureRecordType)

    def resolve_currentTemperature(parent, info, **kwargs):
        return TemperatureRecord.objects.get()


class Subscription(graphene.ObjectType):
    """
    Top level subscription class to expose to GraphQL API
    """

    currentTemperatureSubscribe = TemperatureRecordSubscription.Field()


schema = graphene.Schema(query=Query, subscription=Subscription)


class GraphqlWsConsumer(channels_graphql_ws.GraphqlWsConsumer):
    """
    Channels WebSocket consumer which provides GraphQL API
    """

    schema = schema
