import pytest
import json

from django.core.management import call_command

from channels.testing import WebsocketCommunicator
from channels.db import database_sync_to_async
from graphene_django.utils.testing import graphql_query

from service.routing import application
from temperature.models import TemperatureRecord
from temperature.utils import refresh_record_and_notify


@pytest.mark.django_db
def test_temperaturerecord_refresh_value():
    TemperatureRecord.objects.create()
    entry = TemperatureRecord.objects.get()
    entry_value = entry.value
    entry_ts = entry.timestamp
    entry.refresh()
    entry = TemperatureRecord.objects.get()  # Ensures no in memory value is used
    assert entry.value != entry_value and entry.timestamp != entry_ts


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_refresh_temperaturerecord_and_notify():
    """
    Tests subscription to currentTemperatureSubscription
    """

    def create_record():
        return TemperatureRecord.objects.create()

    entry = await database_sync_to_async(create_record)()
    entry_value = entry.value
    entry_ts = entry.timestamp
    communicator = WebsocketCommunicator(
        application, "/graphql", subprotocols=["graphql-ws"]
    )
    connected, subprotocol = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {
            "id": "1",
            "type": "start",
            "payload": {
                "query": "subscription {currentTemperatureSubscribe{temperature{timestamp\nvalue\nunit\n}}}",
                "variables": None,
            },
        }
    )
    response = communicator.receive_from(timeout=60) # GitlabCI can be rather slow, gives it a fair delay
    await database_sync_to_async(refresh_record_and_notify)()
    response = await response
    assert "errors" not in response
    assert "value" in response
    assert "unit" in response
    assert "timestamp" in response
    assert json.loads(response)["payload"]["data"]["currentTemperatureSubscribe"]["temperature"]["value"] != entry_value
    await communicator.disconnect()


@pytest.fixture
def client_query(client):
    """
    Fixture for GraphQL query tests
    """

    def func(*args, **kwargs):
        return graphql_query(*args, **kwargs, client=client, graphql_url="/graphql")

    TemperatureRecord.objects.create()
    response = func(
        """
        query {
          currentTemperature {
                timestamp
                value
                unit
          }
        }
        """
    )
    content = json.loads(response.content)
    return content


@pytest.mark.django_db
def test_temperaturerecord_query_ok(client_query):
    assert "errors" not in client_query


@pytest.mark.django_db
def test_temperaturerecord_query_value(client_query):
    assert (
        client_query["data"]["currentTemperature"]["value"] > -273
        and client_query["data"]["currentTemperature"]["value"] < 5505
    )


@pytest.mark.django_db
def test_temperaturerecord_query_unit(client_query):
    assert client_query["data"]["currentTemperature"]["unit"] == "Celcius"
